// Message:
// {route: 'login', data: {login: 'login', password: 'password'}, requestId: 0}
// {route: 'sendMessage', data: {message: 'message'}, requestId: 0}
// {route: 'register', data: {login: 'login', password: 'password'}, requestId: 0}
const net = require('net');
const readline = require('readline');
const uid = require('uuid');
const util = require('util');

const requestHandlers = new Map();

let login, password;

const UserRegStatus = Object.freeze({
        UNREGISTERED: 0,
        REGISTER_STATE: 1,
        LOGGED_IN: 2
    }
);

let registrationStatus = UserRegStatus.UNREGISTERED;

const createTextMessage = function(message) {
    return {
        route: 'sendMessage',
        data: {
            message: message
        },
        requestId: uid.v4()
    };
};

const createLoginRequest = function(login, password) {
    let msg = {
        route: 'login',
        data: {
            login: login,
            password: password
        },
        requestId: uid.v4()
    };
    requestHandlers.set(msg.requestId, onLoginInfo);
    return msg;
};

const createRegisterRequest = function (login, password) {
    let message = {
        route: 'register',
        data: {
            login: login,
            password: password
        },
        requestId: uid.v4()
    };
    requestHandlers.set(message.requestId, onRegisterInfo);
    return message;
};

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
const connection = new net.Socket();

const onLoginInfo = function(data, uid) {
    let info = data;
    let result;
    if (info.isAuth) {
        console.log('Connected');
        registrationStatus = UserRegStatus.LOGGED_IN;
        createEventLoop();
        result = true;
    }
    else {
        //connection.on('data', onRegisterInfo);
        registrationStatus = UserRegStatus.REGISTER_STATE;
        connection.write(JSON.stringify(createRegisterRequest(login, password)));
        result = false;
    }
    requestHandlers.delete(uid);
    return result;
};

const onRegisterInfo = function(data, uid) {
    let info = data;
    if (!info.isAuth) {
        connection.end(() => console.log('Register cannot be performed (user already exists or wrong password), connection closed\n'));
        return false;
    }

    console.log('Register succeeded\n');
    registrationStatus = UserRegStatus.LOGGED_IN;
    requestHandlers.delete(uid);
    createEventLoop();
    return true;
};

const createEventLoop = function() {
    rl.on('line', line => {
        connection.write(JSON.stringify(createTextMessage(line)));
    });
    /*connection.on('data', data => {
        let stringData = new TextDecoder("utf-8").decode(data);
        let messageObject = JSON.parse(stringData);
        if (messageObject.type === 'text') {
            console.log(`${messageObject.data.username}: ${messageObject.data.message}`);
        }
    });*/
};

connection.on('connect', () => {
    rl.question('Enter your login and password', answer => {
        let authInfo = answer.split(' ');
        if (authInfo.length < 2) {
            console.log('Cant find password, pls print it in:)');
            connection.end(() => {});
        }
        login = authInfo[0];
        password = authInfo[1];
        let request = createLoginRequest(login, password);

        //connection.on('data', onLoginInfo);
        connection.write(JSON.stringify(request));
    });
});

// json data = {type: 'text', data: {username: 'username', message: 'message'}}
// json response = {type: 'response', data: {requestId: 0, status: success | fail}}
connection.on('data', data => {
    let stringData = new TextDecoder("utf-8").decode(data);
    let messageObject = JSON.parse(stringData);

    if (registrationStatus !== UserRegStatus.LOGGED_IN) {
        if (messageObject.type !== 'response') return;
        let handler = requestHandlers.get(messageObject.data.requestId);
        handler({isAuth: messageObject.data.status === 'success'}, messageObject.data.requestId);
    }
    else {
        if (messageObject.type === 'response') return;
        console.log(`${messageObject.data.username}: ${messageObject.data.message}`);
    }

    /*let stringData = new TextDecoder("utf-8").decode(data);
    let messageObject = JSON.parse(stringData);
    if (messageObject.type === 'response') {
        let result = requestHandlers[messageObject.data.requestId]({isAuth: messageObject.data.status === 'success'});
        if (!connection.destroyed && !result) {
            let registerMessage = createRegisterRequest(login, password);
            connection.write(JSON.stringify(registerMessage));
        }
    }*/

});

connection.connect({
    port: 2222,
    host: '127.0.0.1',
    family: 4
});

