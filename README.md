Simple server-client chat application

Requirements:
* NodeJS executable
* Sequilize module

Just run index.js in ./chat directory and then you can connect to the chat server with the client located in ./charclient directory.

Alexander Antipev, 2020