let Sequelize = require('sequelize');
const Model = Sequelize.Model;
const hash = require('./hash');

class User extends Model {}

class Db {
    sequelize = new Sequelize({
       dialect: 'sqlite',
       storage: './users.sqlite'
    });

    constructor() {
        this.sequelize
            .authenticate();
        User.init({
                login: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                password: {
                    type: Sequelize.STRING,
                    allowNull: false
                }
            },
            {
                sequelize: this.sequelize,
                modelName: 'user'
            });
        this.sequelize.sync();
    }

    async findUserByName(name) {
        return User.findOne({where: {login: name}});
    }

    async createUser(name, password) {
        if (await this.findUserByName(name) != null) return false;
        await User.create({login: name, password: password});
        return true;
    }
}

module.exports = {
    UserDb: Db
};
