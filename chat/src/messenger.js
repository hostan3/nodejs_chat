// Message:
// {route: 'register', data: {login: 'login', password: 'password'}, requestId: 0}
// {route: 'sendMessage', data: {message: 'message'}, requestId: 0}

class Messenger {
    server = require('net').createServer();
    authorizer = require('./authorizer').createAuthorizer();
    connections = new Map();

    constructor() {
        let currentObject = this;

        this.router = require('./router').createRouter(
            this.login(currentObject),
            this.sendMessage(currentObject),
            this.register(currentObject)
        );

        this.server.on('connection', this.handleConnection(currentObject));
        this.server.on('listening', this.listenSocket(currentObject));
        this.server.on('error', err => {
           console.log(err);
        });
        this.server.on('close', () => {
            console.log('close');
        });

        this.server.listen(2222, '0.0.0.0');
    }

    // json data = {type: 'text', data: {username: 'username', message: 'message'}}
// json response = {type: 'response', data: {requestId: 0, status: success | fail}}
    login(currentObject, socket, msg) {
        return async (socket, msg) => {
            let isLoginSuccess = await currentObject.authorizer.login(msg.data.login, msg.data.password);


            let response = {
                type: 'response',
                data: {
                    requestId: msg.requestId,
                    status: 'fail'
                }
            };

            if (isLoginSuccess) {
                response.data.status = 'success';
                let conn = currentObject.connections.get(socket);
                conn.isAuth = true;
                conn.nickname = msg.data.login;
            }

            socket.write(JSON.stringify(response));
        }
    }

    sendMessage(currentObject, socket, msg) {
        return (socket, msg) => {
            let otherConnections = currentObject.connections.keys(); // . filter(key => key !== socket)

            if (!currentObject.connections.get(socket).isAuth) return;

            let message = {
                type: 'text',
                data: {
                    username: currentObject.connections.get(socket).nickname,
                    message: msg.data.message
                }
            };

            for (let k of otherConnections) {
                let conn = currentObject.connections.get(k);
                if (conn.nickname === message.data.username) continue;
                k.write(JSON.stringify(message));
            }
        }
    }

    register(currentObject, socket, msg) {
        return async (socket, msg) => {
            let isRegisterSuccess = await currentObject.authorizer.register(msg.data.login, msg.data.password);

            let response = {
                type: 'response',
                data: {
                    requestId: msg.requestId,
                    status: 'fail'
                }
            };

            if (isRegisterSuccess) {
                response.data.status = 'success';
                let conn = currentObject.connections.get(socket);
                conn.isAuth = true;
                conn.nickname = msg.data.login;
            }

            socket.write(JSON.stringify(response));
        }
    }

    // Message:
// {route: 'login', data: {login: 'login', password: 'password'}, requestId: 0}
// {route: 'sendMessage', data: {message: 'message'}, requestId: 0}
// {route: 'register', data: {login: 'login', password: 'password'}, requestId: 0}
    handleConnection(currentObject) {
        return (socket) => {
            console.log(`New connection: ${socket.remoteAddress}:${socket.remotePort} -> ${socket.localAddress}:${socket.localPort}`);
            currentObject.connections.set(socket, {isAuth: false, nickname: undefined});
            socket.on('error', err => {console.log('Closed?');});
            socket.on('data', data => {
                let message = JSON.parse(data);
                let handler = currentObject.router.routesHandlers.get(message.route);
                handler(socket, message);
            });
            socket.on('close', isError => {
                console.log(`Connection with socket closed`);
            });
        };
    }

    listenSocket(currentObject) {
        return () => {
            let serverInfo = currentObject.server.address();
            console.log(`(${serverInfo.family}) Start listening on ${serverInfo.address}:${serverInfo.port}`);
        }
    }
}

module.exports = {
    Messenger: Messenger
};
