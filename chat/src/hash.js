const Crypto = require('crypto');

const createSha256Hash = function (string) {
    let hash = Crypto.createHash('sha256');
    hash.update(string);
    return hash.digest('hex');
};

module.exports = {
    createSha256Hash: createSha256Hash
};
