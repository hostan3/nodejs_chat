class Router {
    constructor(loginHandler, sendMessageHandler, registerHandler) {
        this.routesHandlers = new Map();
        this.routesHandlers.set('login', loginHandler);
        this.routesHandlers.set('sendMessage', sendMessageHandler);
        this.routesHandlers.set('register', registerHandler);
    }
}

module.exports = {
    createRouter: (loginHandler, sendMessageHandler, registerHandler) => new Router(loginHandler, sendMessageHandler, registerHandler)
};