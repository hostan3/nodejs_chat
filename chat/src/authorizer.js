const db = require('./dbutil');
const hash = require('./hash');

class Authorizer {
    userRepository = new db.UserDb();

    async register(username, password) {
        let passwordHash = hash.createSha256Hash(password);
        return await this.userRepository.createUser(username, passwordHash);
    }

    async login(username, password) {
        let user = await this.userRepository.findUserByName(username);
        if (user == null) return false;
        let passwordHash = hash.createSha256Hash(password);
        return passwordHash === user.password;
    }
}

module.exports = {
    createAuthorizer: () => new Authorizer()
};
